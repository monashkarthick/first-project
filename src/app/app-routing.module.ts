import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckTutorial } from './providers/check-tutorial.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/tutorial',
    pathMatch: 'full'
  },
  {
    path: 'account',
    loadChildren: () => import('./pages/account/account.module').then(m => m.AccountModule)
  },
  {
    path: 'support',
    loadChildren: () => import('./pages/support/support.module').then(m => m.SupportModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./pages/signup/signup.module').then(m => m.SignUpModule)
  },
  {
    path: 'app',
    loadChildren: () => import('./pages/tabs-page/tabs-page.module').then(m => m.TabsModule)
  },
  {
    path: 'tutorial',
    loadChildren: () => import('./pages/tutorial/tutorial.module').then(m => m.TutorialModule),
    canLoad: [CheckTutorial]
  },
  {
    path: 'forgotpas',
    loadChildren: () => import('./pages/forgotpas/forgotpas.module').then( m => m.ForgotpasPageModule)
  },
  {
    path: 'forgotpasw',
    loadChildren: () => import('./pages/forgotpasw/forgotpasw.module').then( m => m.ForgotpaswPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'individual',
    loadChildren: () => import('./pages/individual/individual.module').then( m => m.IndividualPageModule)
  },
  {
    path: 'business',
    loadChildren: () => import('./pages/business/business.module').then( m => m.BusinessPageModule)
  },
  {
    path: 'provider',
    loadChildren: () => import('./pages/provider/provider.module').then( m => m.ProviderPageModule)
  },
  {
    path: 'providercheck',
    loadChildren: () => import('./pages/providercheck/providercheck.module').then( m => m.ProvidercheckPageModule)
  },
  {
    path: 'providertype',
    loadChildren: () => import('./pages/providertype/providertype.module').then( m => m.ProvidertypePageModule)
  },
  {
    path: 'logincred',
    loadChildren: () => import('./pages/logincred/logincred.module').then( m => m.LogincredPageModule)
  },
  {
    path: 'finishset',
    loadChildren: () => import('./pages/finishset/finishset.module').then( m => m.FinishsetPageModule)
  },
  {
    path: 'indiuser',
    loadChildren: () => import('./pages/indiuser/indiuser.module').then( m => m.IndiuserPageModule)
  },
  {
    path: 'indiusertype',
    loadChildren: () => import('./pages/indiusertype/indiusertype.module').then( m => m.IndiusertypePageModule)
  },
  {
    path: 'indinsure',
    loadChildren: () => import('./pages/indinsure/indinsure.module').then( m => m.IndinsurePageModule)
  },
  {
    path: 'indipdf',
    loadChildren: () => import('./pages/indipdf/indipdf.module').then( m => m.IndipdfPageModule)
  },
  {
    path: 'indiloadpdf',
    loadChildren: () => import('./pages/indiloadpdf/indiloadpdf.module').then( m => m.IndiloadpdfPageModule)
  },
  {
    path: 'indilogincred',
    loadChildren: () => import('./pages/indilogincred/indilogincred.module').then( m => m.IndilogincredPageModule)
  },
  {
    path: 'indifinish',
    loadChildren: () => import('./pages/indifinish/indifinish.module').then( m => m.IndifinishPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'mathome',
    loadChildren: () => import('./pages/mathome/mathome.module').then( m => m.MathomePageModule)
  },
  {
    path: 'facreport',
    loadChildren: () => import('./pages/facreport/facreport.module').then( m => m.FacreportPageModule)
  },
  {
    path: 'facreptoday',
    loadChildren: () => import('./pages/facreptoday/facreptoday.module').then( m => m.FacreptodayPageModule)
  },
  {
    path: 'facreplast6',
    loadChildren: () => import('./pages/facreplast6/facreplast6.module').then( m => m.Facreplast6PageModule)
  },
  {
    path: 'facreplast30',
    loadChildren: () => import('./pages/facreplast30/facreplast30.module').then( m => m.Facreplast30PageModule)
  },
  {
    path: 'facreplast5y',
    loadChildren: () => import('./pages/facreplast5y/facreplast5y.module').then( m => m.Facreplast5yPageModule)
  },
  {
    path: 'facsearch',
    loadChildren: () => import('./pages/facsearch/facsearch.module').then( m => m.FacsearchPageModule)
  },
  {
    path: 'facpatient',
    loadChildren: () => import('./pages/facpatient/facpatient.module').then( m => m.FacpatientPageModule)
  },
  {
    path: 'facaddmot',
    loadChildren: () => import('./pages/facaddmot/facaddmot.module').then( m => m.FacaddmotPageModule)
  },
  {
    path: 'facaddcild',
    loadChildren: () => import('./pages/facaddcild/facaddcild.module').then( m => m.FacaddcildPageModule)
  },
  {
    path: 'facmotpro',
    loadChildren: () => import('./pages/facmotpro/facmotpro.module').then( m => m.FacmotproPageModule)
  },
  {
    path: 'facprochild',
    loadChildren: () => import('./pages/facprochild/facprochild.module').then( m => m.FacprochildPageModule)
  },
  {
    path: 'facdismot',
    loadChildren: () => import('./pages/facdismot/facdismot.module').then( m => m.FacdismotPageModule)
  },
  {
    path: 'facmother',
    loadChildren: () => import('./pages/facmother/facmother.module').then( m => m.FacmotherPageModule)
  },
  {
    path: 'facchild',
    loadChildren: () => import('./pages/facchild/facchild.module').then( m => m.FacchildPageModule)
  },
  {
    path: 'facpromot',
    loadChildren: () => import('./pages/facpromot/facpromot.module').then( m => m.FacpromotPageModule)
  },
  {
    path: 'facadmot',
    loadChildren: () => import('./pages/facadmot/facadmot.module').then( m => m.FacadmotPageModule)
  },
  {
    path: 'facmotnote',
    loadChildren: () => import('./pages/facmotnote/facmotnote.module').then( m => m.FacmotnotePageModule)
  },
  {
    path: 'facnotemot',
    loadChildren: () => import('./pages/facnotemot/facnotemot.module').then( m => m.FacnotemotPageModule)
  },
  {
    path: 'patient',
    loadChildren: () => import('./pages/patient/patient.module').then( m => m.PatientPageModule)
  },
  {
    path: 'child1',
    loadChildren: () => import('./pages/child1/child1.module').then( m => m.Child1PageModule)
  },
  {
    path: 'child2',
    loadChildren: () => import('./pages/child2/child2.module').then( m => m.Child2PageModule)
  },
  {
    path: 'patientlist',
    loadChildren: () => import('./pages/patientlist/patientlist.module').then( m => m.PatientlistPageModule)
  },
  {
    path: 'patientnotes',
    loadChildren: () => import('./pages/patientnotes/patientnotes.module').then( m => m.PatientnotesPageModule)
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
