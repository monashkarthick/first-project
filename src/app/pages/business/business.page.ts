import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { UserOptions, UserOptionss } from '../../interfaces/user-options';
import {NavController, MenuController } from '@ionic/angular';

@Component({
  selector: 'business',
  templateUrl: './business.page.html',
  styleUrls: ['./business.page.scss'],
})
export class BusinessPage implements OnInit {
  business: UserOptionss = { username: '', lastname: '', email:'', number:'', address:''};
  submitted = false;

  constructor(
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public userData: UserData,
    public router: Router
  ) { }

  ngOnInit() {
    //// this.menuCtrl.enable(false);
  }
  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.userData.login(this.business.username);
      this.router.navigateByUrl('/provider');
    }
  }

}
