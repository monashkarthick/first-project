import { Component, OnInit } from '@angular/core';
import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'child1',
  templateUrl: './child1.page.html',
  styleUrls: ['./child1.page.scss'],
})
export class Child1Page implements OnInit {

  constructor(public menuCtrl: MenuController,
    public navCtrl: NavController) { }

  ngOnInit() {
    //// this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
  }

}
