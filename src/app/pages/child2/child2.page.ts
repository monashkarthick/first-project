import { Component, OnInit } from '@angular/core';
import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'child2',
  templateUrl: './child2.page.html',
  styleUrls: ['./child2.page.scss'],
})
export class Child2Page implements OnInit {

  constructor(public menuCtrl: MenuController,
    public navCtrl: NavController) { }

  ngOnInit() {
    //// this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
  }

}
