import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacaddcildPage } from './facaddcild.page';

const routes: Routes = [
  {
    path: '',
    component: FacaddcildPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacaddcildPageRoutingModule {}
