import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacaddcildPageRoutingModule } from './facaddcild-routing.module';

import { FacaddcildPage } from './facaddcild.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacaddcildPageRoutingModule
  ],
  declarations: [FacaddcildPage]
})
export class FacaddcildPageModule {}
