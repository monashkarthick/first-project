import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacaddcildPage } from './facaddcild.page';

describe('FacaddcildPage', () => {
  let component: FacaddcildPage;
  let fixture: ComponentFixture<FacaddcildPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacaddcildPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacaddcildPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
