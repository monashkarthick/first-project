import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { UserOptions, UserAdd } from '../../interfaces/user-options';
import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'facaddcild',
  templateUrl: './facaddcild.page.html',
  styleUrls: ['./facaddcild.page.scss'],
})
export class FacaddcildPage implements OnInit {
  facadd: UserAdd = { name: '', group: '', number:'', gender:'', date:'', time:''};
  submitted = false;
  constructor(
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public userData: UserData,
    public router: Router
  ) { }

  ngOnInit() {
    //// this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
    //  //// this.menuCtrl.enable(false);
  }
  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.userData.login(this.facadd.name);
      this.router.navigateByUrl('/facprochild');
    }
  }
}
