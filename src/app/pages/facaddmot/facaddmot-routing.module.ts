import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacaddmotPage } from './facaddmot.page';

const routes: Routes = [
  {
    path: '',
    component: FacaddmotPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacaddmotPageRoutingModule {}
