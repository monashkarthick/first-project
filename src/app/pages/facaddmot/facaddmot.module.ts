import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacaddmotPageRoutingModule } from './facaddmot-routing.module';

import { FacaddmotPage } from './facaddmot.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacaddmotPageRoutingModule
  ],
  declarations: [FacaddmotPage]
})
export class FacaddmotPageModule {}
