import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacaddmotPage } from './facaddmot.page';

describe('FacaddmotPage', () => {
  let component: FacaddmotPage;
  let fixture: ComponentFixture<FacaddmotPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacaddmotPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacaddmotPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
