import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacadmotPage } from './facadmot.page';

const routes: Routes = [
  {
    path: '',
    component: FacadmotPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacadmotPageRoutingModule {}
