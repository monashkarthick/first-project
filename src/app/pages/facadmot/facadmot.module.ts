import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacadmotPageRoutingModule } from './facadmot-routing.module';

import { FacadmotPage } from './facadmot.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacadmotPageRoutingModule
  ],
  declarations: [FacadmotPage]
})
export class FacadmotPageModule {}
