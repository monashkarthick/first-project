import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacadmotPage } from './facadmot.page';

describe('FacadmotPage', () => {
  let component: FacadmotPage;
  let fixture: ComponentFixture<FacadmotPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacadmotPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacadmotPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
