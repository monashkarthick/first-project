import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { UserOptions, UserPro } from '../../interfaces/user-options';
import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'facadmot',
  templateUrl: './facadmot.page.html',
  styleUrls: ['./facadmot.page.scss'],
})
export class FacadmotPage implements OnInit {

  facadmot:  UserPro =  { name: '', number: '', anaesname: '', pressure: '',  height: '', weight: '', sugar: '', notes:'', outcome:'' };
  submitted = false;

  constructor(public menuCtrl: MenuController,
    public navCtrl: NavController,
    public userData: UserData,
    public router: Router)
   { }

  ngOnInit() {
    //// this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
  }
  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.userData.login(this.facadmot.name);
      this.router.navigateByUrl('/facpatient');
    }
  }
    

}
