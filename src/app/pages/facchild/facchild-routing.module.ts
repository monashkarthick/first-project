import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacchildPage } from './facchild.page';

const routes: Routes = [
  {
    path: '',
    component: FacchildPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacchildPageRoutingModule {}
