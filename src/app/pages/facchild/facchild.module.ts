import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacchildPageRoutingModule } from './facchild-routing.module';

import { FacchildPage } from './facchild.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacchildPageRoutingModule
  ],
  declarations: [FacchildPage]
})
export class FacchildPageModule {}
