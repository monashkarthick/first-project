import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacchildPage } from './facchild.page';

describe('FacchildPage', () => {
  let component: FacchildPage;
  let fixture: ComponentFixture<FacchildPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacchildPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacchildPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
