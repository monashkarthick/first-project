import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacdismotPage } from './facdismot.page';

const routes: Routes = [
  {
    path: '',
    component: FacdismotPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacdismotPageRoutingModule {}
