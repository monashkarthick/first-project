import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacdismotPageRoutingModule } from './facdismot-routing.module';

import { FacdismotPage } from './facdismot.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacdismotPageRoutingModule
  ],
  declarations: [FacdismotPage]
})
export class FacdismotPageModule {}
