import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacdismotPage } from './facdismot.page';

describe('FacdismotPage', () => {
  let component: FacdismotPage;
  let fixture: ComponentFixture<FacdismotPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacdismotPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacdismotPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
