import { Component, OnInit } from '@angular/core';
import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'facdismot',
  templateUrl: './facdismot.page.html',
  styleUrls: ['./facdismot.page.scss'],
})
export class FacdismotPage implements OnInit {

  constructor(
    public menuCtrl: MenuController,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
    //// this.menuCtrl.enable(false);
    // this.menuCtrl.swipeEnable(false);
  }

  facmother(){
    this.navCtrl.navigateRoot('/facmother');
  }
  facchild(){
    this.navCtrl.navigateRoot('/facchild');
  }

discharge()
{
  this.navCtrl.navigateRoot('/home');
  }
}
