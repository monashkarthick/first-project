import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacmotherPage } from './facmother.page';

const routes: Routes = [
  {
    path: '',
    component: FacmotherPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacmotherPageRoutingModule {}
