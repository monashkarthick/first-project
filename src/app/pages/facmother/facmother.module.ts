import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacmotherPageRoutingModule } from './facmother-routing.module';

import { FacmotherPage } from './facmother.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacmotherPageRoutingModule
  ],
  declarations: [FacmotherPage]
})
export class FacmotherPageModule {}
