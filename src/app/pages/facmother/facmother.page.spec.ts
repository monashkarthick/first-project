import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacmotherPage } from './facmother.page';

describe('FacmotherPage', () => {
  let component: FacmotherPage;
  let fixture: ComponentFixture<FacmotherPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacmotherPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacmotherPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
