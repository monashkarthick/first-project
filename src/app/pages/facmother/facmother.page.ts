import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { UserOptions } from '../../interfaces/user-options';
import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'facmother',
  templateUrl: './facmother.page.html',
  styleUrls: ['./facmother.page.scss'],
})
export class FacmotherPage implements OnInit {

  constructor(public menuCtrl: MenuController,
    public navCtrl: NavController,
    public userData: UserData,
    public router: Router) { }

  ngOnInit() {
   // this.menuCtrl.enable(false);
  }
  
  facadmot(){
    this.navCtrl.navigateRoot('/facadmot'); 
  }
  facnotemot(){
    this.navCtrl.navigateRoot('/facnotemot'); 

  }
}
