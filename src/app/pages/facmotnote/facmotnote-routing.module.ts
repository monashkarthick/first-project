import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacmotnotePage } from './facmotnote.page';

const routes: Routes = [
  {
    path: '',
    component: FacmotnotePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacmotnotePageRoutingModule {}
