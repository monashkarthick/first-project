import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacmotnotePageRoutingModule } from './facmotnote-routing.module';

import { FacmotnotePage } from './facmotnote.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacmotnotePageRoutingModule
  ],
  declarations: [FacmotnotePage]
})
export class FacmotnotePageModule {}
