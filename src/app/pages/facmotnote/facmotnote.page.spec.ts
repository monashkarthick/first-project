import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacmotnotePage } from './facmotnote.page';

describe('FacmotnotePage', () => {
  let component: FacmotnotePage;
  let fixture: ComponentFixture<FacmotnotePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacmotnotePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacmotnotePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
