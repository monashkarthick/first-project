import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacmotproPage } from './facmotpro.page';

const routes: Routes = [
  {
    path: '',
    component: FacmotproPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacmotproPageRoutingModule {}
