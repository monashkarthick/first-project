import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacmotproPageRoutingModule } from './facmotpro-routing.module';

import { FacmotproPage } from './facmotpro.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacmotproPageRoutingModule
  ],
  declarations: [FacmotproPage]
})
export class FacmotproPageModule {}
