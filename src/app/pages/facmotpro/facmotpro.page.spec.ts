import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacmotproPage } from './facmotpro.page';

describe('FacmotproPage', () => {
  let component: FacmotproPage;
  let fixture: ComponentFixture<FacmotproPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacmotproPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacmotproPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
