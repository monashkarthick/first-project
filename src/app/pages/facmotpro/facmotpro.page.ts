import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { UserOptions, UserMot } from '../../interfaces/user-options';
import {NavController, MenuController } from '@ionic/angular';
import {BarcodeScannerOptions, BarcodeScanner } from "@ionic-native/barcode-scanner/ngx";
@Component({
  selector: 'facmotpro',
  templateUrl: './facmotpro.page.html',
  styleUrls: ['./facmotpro.page.scss'],
})
export class FacmotproPage implements OnInit {
  encodeData: any;
  scannedData: {};
  barcodeScannerOptions: BarcodeScannerOptions;
  mother: UserMot = { number: '', name: '', wname: '', positive: '',  date: '', age: '', time: '',outcome: '' };
  submitted = false;

  constructor(
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public userData: UserData,
    public router: Router,
    private barcodeScanner: BarcodeScanner
  )  {
    this.encodeData = "https://www.FreakyJolly.com";
    //Options
    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };
  }
 

  ngOnInit() {
    // this.menuCtrl.enable(false);
  }
  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.userData.login(this.mother.number);
      this.router.navigateByUrl('/facpatient');
    }
  }
  encodedText() {
    this.barcodeScanner
      .encode(this.barcodeScanner.Encode.TEXT_TYPE, this.encodeData)
      .then(
        encodedData => {
          console.log(encodedData);
          this.encodeData = encodedData;
        },
        err => {
          console.log("Error occured : " + err);
        }
      );
  }

}
