import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacnotemotPage } from './facnotemot.page';

const routes: Routes = [
  {
    path: '',
    component: FacnotemotPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacnotemotPageRoutingModule {}
