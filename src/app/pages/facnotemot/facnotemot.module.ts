import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacnotemotPageRoutingModule } from './facnotemot-routing.module';

import { FacnotemotPage } from './facnotemot.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacnotemotPageRoutingModule
  ],
  declarations: [FacnotemotPage]
})
export class FacnotemotPageModule {}
