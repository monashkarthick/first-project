import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacnotemotPage } from './facnotemot.page';

describe('FacnotemotPage', () => {
  let component: FacnotemotPage;
  let fixture: ComponentFixture<FacnotemotPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacnotemotPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacnotemotPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
