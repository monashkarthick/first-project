import { Component, OnInit } from '@angular/core';
import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'facnotemot',
  templateUrl: './facnotemot.page.html',
  styleUrls: ['./facnotemot.page.scss'],
})
export class FacnotemotPage implements OnInit {

  constructor(
    public menuCtrl: MenuController,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
   // this.menuCtrl.enable(false);
    this.menuCtrl.swipeEnable(false);
  }

}
