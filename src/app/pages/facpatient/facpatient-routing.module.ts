import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacpatientPage } from './facpatient.page';

const routes: Routes = [
  {
    path: '',
    component: FacpatientPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacpatientPageRoutingModule {}
