import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacpatientPageRoutingModule } from './facpatient-routing.module';

import { FacpatientPage } from './facpatient.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacpatientPageRoutingModule
  ],
  declarations: [FacpatientPage]
})
export class FacpatientPageModule {}
