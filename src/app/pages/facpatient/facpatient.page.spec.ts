import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacpatientPage } from './facpatient.page';

describe('FacpatientPage', () => {
  let component: FacpatientPage;
  let fixture: ComponentFixture<FacpatientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacpatientPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacpatientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
