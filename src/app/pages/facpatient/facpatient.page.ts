import { Component, OnInit } from '@angular/core';
import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'facpatient',
  templateUrl: './facpatient.page.html',
  styleUrls: ['./facpatient.page.scss'],
})
export class FacpatientPage implements OnInit {

  constructor(
    public menuCtrl: MenuController,
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
   // this.menuCtrl.enable(false);
    this.menuCtrl.swipeEnable(false);
  }
 child(){
    this.navCtrl.navigateRoot('/facaddcild');
  }
  mother()
  {
    this.navCtrl.navigateRoot('/facaddmot');
  }
 facsearch()
  {
    this.navCtrl.navigateRoot('/facsearch');
  }
  login(){
    this.navCtrl.navigateRoot('/home');
  }
}
