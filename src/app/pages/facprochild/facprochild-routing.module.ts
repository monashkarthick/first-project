import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacprochildPage } from './facprochild.page';

const routes: Routes = [
  {
    path: '',
    component: FacprochildPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacprochildPageRoutingModule {}
