import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacprochildPageRoutingModule } from './facprochild-routing.module';

import { FacprochildPage } from './facprochild.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacprochildPageRoutingModule
  ],
  declarations: [FacprochildPage]
})
export class FacprochildPageModule {}
