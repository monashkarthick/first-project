import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacprochildPage } from './facprochild.page';

describe('FacprochildPage', () => {
  let component: FacprochildPage;
  let fixture: ComponentFixture<FacprochildPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacprochildPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacprochildPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
