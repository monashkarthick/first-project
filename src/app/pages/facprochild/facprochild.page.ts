import { Component, OnInit } from '@angular/core';
import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'facprochild',
  templateUrl: './facprochild.page.html',
  styleUrls: ['./facprochild.page.scss'],
})
export class FacprochildPage implements OnInit {

  constructor(
    public menuCtrl: MenuController,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
   // this.menuCtrl.enable(false);
  }
  facchild(){
    this.navCtrl.navigateRoot('/facchild');
  }
  facdismot(){
    this.navCtrl.navigateRoot('/facdismot');
    }
facmother(){
  this.navCtrl.navigateRoot('/facmother');
}
}
