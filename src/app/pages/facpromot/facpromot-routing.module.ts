import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacpromotPage } from './facpromot.page';

const routes: Routes = [
  {
    path: '',
    component: FacpromotPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacpromotPageRoutingModule {}
