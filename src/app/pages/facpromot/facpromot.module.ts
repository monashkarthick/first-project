import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacpromotPageRoutingModule } from './facpromot-routing.module';

import { FacpromotPage } from './facpromot.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacpromotPageRoutingModule
  ],
  declarations: [FacpromotPage]
})
export class FacpromotPageModule {}
