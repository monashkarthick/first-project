import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacpromotPage } from './facpromot.page';

describe('FacpromotPage', () => {
  let component: FacpromotPage;
  let fixture: ComponentFixture<FacpromotPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacpromotPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacpromotPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
