import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Facreplast30Page } from './facreplast30.page';

const routes: Routes = [
  {
    path: '',
    component: Facreplast30Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Facreplast30PageRoutingModule {}
