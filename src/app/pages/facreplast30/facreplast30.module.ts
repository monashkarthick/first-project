import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Facreplast30PageRoutingModule } from './facreplast30-routing.module';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { Facreplast30Page } from './facreplast30.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2GoogleChartsModule,
    Facreplast30PageRoutingModule
  ],
  declarations: [Facreplast30Page]
})
export class Facreplast30PageModule {}
