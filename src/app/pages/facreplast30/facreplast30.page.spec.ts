import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Facreplast30Page } from './facreplast30.page';

describe('Facreplast30Page', () => {
  let component: Facreplast30Page;
  let fixture: ComponentFixture<Facreplast30Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Facreplast30Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Facreplast30Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
