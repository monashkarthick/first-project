import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Facreplast5yPage } from './facreplast5y.page';

const routes: Routes = [
  {
    path: '',
    component: Facreplast5yPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Facreplast5yPageRoutingModule {}
