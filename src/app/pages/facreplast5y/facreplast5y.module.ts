import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { Facreplast5yPageRoutingModule } from './facreplast5y-routing.module';
import { Facreplast5yPage } from './facreplast5y.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2GoogleChartsModule,
    Facreplast5yPageRoutingModule
  ],
  declarations: [Facreplast5yPage]
})
export class Facreplast5yPageModule {}
