import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Facreplast5yPage } from './facreplast5y.page';

describe('Facreplast5yPage', () => {
  let component: Facreplast5yPage;
  let fixture: ComponentFixture<Facreplast5yPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Facreplast5yPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Facreplast5yPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
