import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Facreplast6Page } from './facreplast6.page';

const routes: Routes = [
  {
    path: '',
    component: Facreplast6Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Facreplast6PageRoutingModule {}
