import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { Facreplast6PageRoutingModule } from './facreplast6-routing.module';
import { Facreplast6Page } from './facreplast6.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2GoogleChartsModule,
    Facreplast6PageRoutingModule
  ],
  declarations: [Facreplast6Page]
})
export class Facreplast6PageModule {}
