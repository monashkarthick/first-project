import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Facreplast6Page } from './facreplast6.page';

describe('Facreplast6Page', () => {
  let component: Facreplast6Page;
  let fixture: ComponentFixture<Facreplast6Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Facreplast6Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Facreplast6Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
