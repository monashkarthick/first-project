import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacreportPage } from './facreport.page';

const routes: Routes = [
  {
    path: '',
    component: FacreportPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacreportPageRoutingModule {}
