import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacreportPageRoutingModule } from './facreport-routing.module';

import { FacreportPage } from './facreport.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacreportPageRoutingModule
  ],
  declarations: [FacreportPage]
})
export class FacreportPageModule {}
