import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacreportPage } from './facreport.page';

describe('FacreportPage', () => {
  let component: FacreportPage;
  let fixture: ComponentFixture<FacreportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacreportPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacreportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
