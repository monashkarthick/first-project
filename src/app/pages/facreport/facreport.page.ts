import { Component, OnInit } from '@angular/core';
import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'facreport',
  templateUrl: './facreport.page.html',
  styleUrls: ['./facreport.page.scss'],
})
export class FacreportPage implements OnInit {

  constructor(public menuCtrl: MenuController,
    public navCtrl: NavController) { }

  ngOnInit() {
   // this.menuCtrl.enable(false);
  }
  today(){
    this.navCtrl.navigateRoot('/facreptoday');
    }
  last6mon(){
    this.navCtrl.navigateRoot('/facreplast6');
    }
  last30(){
    this.navCtrl.navigateRoot('/facreplast30');
    }
  last5yea(){
    this.navCtrl.navigateRoot('/facreplast5y');
    }

}
