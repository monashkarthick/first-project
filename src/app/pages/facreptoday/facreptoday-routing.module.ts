import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacreptodayPage } from './facreptoday.page';

const routes: Routes = [
  {
    path: '',
    component: FacreptodayPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacreptodayPageRoutingModule {}
