import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacreptodayPageRoutingModule } from './facreptoday-routing.module';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { FacreptodayPage } from './facreptoday.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    Ng2GoogleChartsModule,
    IonicModule,
    FacreptodayPageRoutingModule
  ],
  declarations: [FacreptodayPage]
})
export class FacreptodayPageModule {}
