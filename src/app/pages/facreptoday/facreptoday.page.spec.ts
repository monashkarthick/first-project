import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacreptodayPage } from './facreptoday.page';

describe('FacreptodayPage', () => {
  let component: FacreptodayPage;
  let fixture: ComponentFixture<FacreptodayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacreptodayPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacreptodayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
