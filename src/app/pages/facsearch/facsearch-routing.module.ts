import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacsearchPage } from './facsearch.page';

const routes: Routes = [
  {
    path: '',
    component: FacsearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacsearchPageRoutingModule {}
