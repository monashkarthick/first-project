import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacsearchPageRoutingModule } from './facsearch-routing.module';

import { FacsearchPage } from './facsearch.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacsearchPageRoutingModule
  ],
  declarations: [FacsearchPage]
})
export class FacsearchPageModule {}
