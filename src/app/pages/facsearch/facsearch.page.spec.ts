import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacsearchPage } from './facsearch.page';

describe('FacsearchPage', () => {
  let component: FacsearchPage;
  let fixture: ComponentFixture<FacsearchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacsearchPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacsearchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
