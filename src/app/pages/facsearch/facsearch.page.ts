import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { UserOptions } from '../../interfaces/user-options';
import {NavController, MenuController } from '@ionic/angular';

@Component({
  selector: 'facsearch',
  templateUrl: './facsearch.page.html',
  styleUrls: ['./facsearch.page.scss'],
})

export class FacsearchPage implements OnInit {

  // login: UserOptions = { username: '', password: '' };
  submitted = false;

   /**
    * @name patients
    * @type {Array}
    * @public
    * @address     Array for holding individual object data of development patients
    */
   public patients : Array<any>;

  constructor(
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public userData: UserData,
    public router: Router
  ) { }

  /**
    * Triggered when template view has completed loading
    *
    * @public
    * @method ionViewDidLoad
    * @return {None}
    */

   ionViewDidLoad()
   {
      this.declarepatients();
   }

    /**
    * Populates the patients array with different patients
    *
    * @public
    * @method declarepatients
    * @return {None}
    */
   declarepatients() : void
   {
       this.patients = [
          {
            name : 'Akshaya',
            dateofbirth: '11/03/1998',
            address: '6/45,sankara bathan street,Ayanavaram,Chennai-12',
            image: '/assets/img/mother.jpg',
            type : 'framework'
          },
          {
            name : 'Aparna',
            dateofbirth: '18/11/1991',
            address: 'D28,75,IIT,Guindy,Chennai-36',
            image: '/assets/img/mother.jpg',
            type : 'framework'
          },
          {
            name : 'Fathima',
            dateofbirth: '22/05/1994',
            address: '37/38 A,Muthulakshmi street,Jothi Nagar,Salem-102',
            image: '/assets/img/mother.jpg',
            type : 'browser'
          },
          {
            name : 'Charu',
            dateofbirth: '05/05/1996',
            address: 'C 67, BHEL township,Ranipet-406',
            image: '/assets/img/mother.jpg',
            type : 'browser'
          },
          {
            name : 'Iliyana',
            dateofbirth: '29/02/1995',
            address: '12,Sahana Phase 3, Maruthi street,Madipakkam,Chennai-91',
            image: '/assets/img/mother.jpg',
            type : 'browser'
          },
          {
            name : 'Hema',
            dateofbirth: '25/11/1997',
            address: '8,Venkatasa perumal south street,Karaikal-602',
            image: '/assets/img/mother.jpg',
            type : 'language'
          },
          {
            name : 'Chandrika',
            dateofbirth: '01/07/1993',
            address: '6/45,sankara bathan street,Ayanavaram,Chennai-12',
            image: '/assets/img/mother.jpg',
            type : 'language'
          },
          {
            name : 'Swetha',
            dateofbirth: '15/04/1992',
            address: '8,Venkatasa perumal south street,Karaikal-602',
            image: '/assets/img/mother.jpg',
            type : 'library'
          },
          {
            name : 'Thamana',
            dateofbirth: '22/05/1994',
            address: '12,Sahana Phase 3, Maruthi street,Madipakkam,Chennai-91',
            image: '/assets/img/mother.jpg',
            type : 'language'
          },
          {
            name : 'Indhu',
            dateofbirth: '18/11/1991',
            address: 'C 67, BHEL township,Ranipet-406',
            image: '/assets/img/mother.jpg',
            type : 'library'
          },
          {
            name : 'Catherine',
            dateofbirth: '11/03/1998',
            address: 'D28,75,IIT,Guindy,Chennai-36',
            image: '/assets/img/mother.jpg',
            type : 'OS'
          },
          {
            name : 'Banumathi',
            dateofbirth: '22/05/1994',
            address: '37/38 A,Muthulakshmi street,Jothi Nagar,Salem-102',
            image: '/assets/img/mother.jpg',
            type : 'OS'
          },
          {
            name : 'Wamini',
            dateofbirth: '15/04/1992',
            address: '6/45,sankara bathan street,Ayanavaram,Chennai-12',
            image: '/assets/img/mother.jpg',
            type : 'OS'
          },
          {
            name : 'Pravalika',
            dateofbirth: '18/11/1991',
            address: '8,Venkatasa perumal south street,Karaikal-602',
            image: '/assets/img/mother.jpg',
            type : 'language'
          },
          {
            name : 'Malarvizhi',
            dateofbirth: '25/11/1997',
            address: 'D28,75,IIT,Guindy,Chennai-36',
            image: '/assets/img/mother.jpg',
            type : 'database'
          },
          {
            name : 'Mani Megalai',
            dateofbirth: '01/07/1993',
            address: 'C 67, BHEL township,Ranipet-406',
            image: '/assets/img/mother.jpg',
            type : 'database'
          },
          {
            name : 'Famira',
            dateofbirth: '29/02/1995',
            address: '12,Sahana Phase 3, Maruthi street,Madipakkam,Chennai-91',
            image: '/assets/img/mother.jpg',
            type : 'platform'
          },
          {
            name : 'Samantha',
            dateofbirth: '25/11/1997',
            address: '37/38 A,Muthulakshmi street,Jothi Nagar,Salem-102',
            image: '/assets/img/mother.jpg',
            type : 'browser'
          },
          {
            name : 'Oviaa',
            dateofbirth: '18/11/1991',
            address: '37/38 A,Muthulakshmi street,Jothi Nagar,Salem-102',
            image: '/assets/img/mother.jpg',
            type : 'browser'
          },
          {
            name : 'Kaviya',
            dateofbirth: '15/04/1992',
            address: '12,Sahana Phase 3, Maruthi street,Madipakkam,Chennai-91',
            image: '/assets/img/mother.jpg',
            type : 'library'
          }];
    }




   /**
    * Filter patients array by keyword/letter
    *
    * @public
    * @method filterpatients
    @ param event     {Object}		The event object emitted by the <ion-searchbar> when input is entered
    * @return {None}
    */
   filterpatients(param : any) : void
   {
      this.declarepatients();


      let val : string 	= param;

      // DON'T filter the patients IF the supplied input is an empty string
      if (val.trim() !== '')
      {
         this.patients = this.patients.filter((item) =>
         {
           return item.name.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.address.toLowerCase().indexOf(val.toLowerCase()) > -1;
         })
      }
   }




   /**
    * Filter patients array by type selected
    *
    * @public
    * @method onFilter
    @ param val     {String}		The string value supplied by the <ion-radio> when selected
    * @return {None}
    */
   onFilter(category : string) : void
   {
      this.declarepatients();

      // Only filter the patients array IF the selection is NOT equal to value of all
      if (category.trim() !== 'all')
      {
         this.patients = this.patients.filter((item) =>
         {
           return item.type.toLowerCase().indexOf(category.toLowerCase()) > -1;
         })
      }
   }

  ngOnInit() {
    // this.menuCtrl.enable(false);
  }
  onLogin() {
    
      this.router.navigateByUrl('/facpatient');
    }

  mother() {
    this.router.navigateByUrl('/facmotpro');
  }

}
