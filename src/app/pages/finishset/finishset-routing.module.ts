import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FinishsetPage } from './finishset.page';

const routes: Routes = [
  {
    path: '',
    component: FinishsetPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FinishsetPageRoutingModule {}
