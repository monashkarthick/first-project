import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FinishsetPageRoutingModule } from './finishset-routing.module';

import { FinishsetPage } from './finishset.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FinishsetPageRoutingModule
  ],
  declarations: [FinishsetPage]
})
export class FinishsetPageModule {}
