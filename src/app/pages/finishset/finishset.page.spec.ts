import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FinishsetPage } from './finishset.page';

describe('FinishsetPage', () => {
  let component: FinishsetPage;
  let fixture: ComponentFixture<FinishsetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishsetPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FinishsetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
