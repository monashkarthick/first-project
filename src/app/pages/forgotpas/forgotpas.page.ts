import { Component, OnInit } from '@angular/core';
import {NavController, MenuController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';


import { UserData } from '../../providers/user-data';

import { UserOptions } from '../../interfaces/user-options';


@Component({
  selector: 'forgotpas',
  templateUrl: './forgotpas.page.html',
  styleUrls: ['./forgotpas.page.scss'],
})
export class ForgotpasPage implements OnInit {
  login: UserOptions = { username: '', password: '' };
  submitted = false;

  constructor(
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public userData: UserData,
    public router: Router
  ) { }

  ngOnInit() {
   // this.menuCtrl.enable(false);
     this.menuCtrl.swipeEnable(false);
    }
    onLogin(form: NgForm) {
      this.submitted = true;
  
      if (form.valid) {
        this.userData.login(this.login.username);
        this.router.navigateByUrl('/forgotpasw');
      }
    }
  
}
