import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ForgotpaswPage } from './forgotpasw.page';

const routes: Routes = [
  {
    path: '',
    component: ForgotpaswPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ForgotpaswPageRoutingModule {}
