import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ForgotpaswPageRoutingModule } from './forgotpasw-routing.module';

import { ForgotpaswPage } from './forgotpasw.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ForgotpaswPageRoutingModule
  ],
  declarations: [ForgotpaswPage]
})
export class ForgotpaswPageModule {}
