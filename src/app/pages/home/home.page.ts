import { Component, OnInit } from '@angular/core';
import {NavController,MenuController} from '@ionic/angular';



@Component({
  selector: 'home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
 

 

  constructor(
    public menuCtrl: MenuController,
    public navCtrl: NavController)
 { }

  ngOnInit() {

  
    // this.menuCtrl.enable(false);
  }
  search() {
    this.navCtrl.navigateRoot('/facsearch');
  }
  report() {
    this.navCtrl.navigateRoot('/facreport');
  }
  logout() {
    this.navCtrl.navigateRoot('/login');
  }
 
  

}