import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndifinishPage } from './indifinish.page';

const routes: Routes = [
  {
    path: '',
    component: IndifinishPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IndifinishPageRoutingModule {}
