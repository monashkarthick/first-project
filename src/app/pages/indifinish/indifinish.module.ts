import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IndifinishPageRoutingModule } from './indifinish-routing.module';

import { IndifinishPage } from './indifinish.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IndifinishPageRoutingModule
  ],
  declarations: [IndifinishPage]
})
export class IndifinishPageModule {}
