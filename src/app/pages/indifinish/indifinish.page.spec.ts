import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IndifinishPage } from './indifinish.page';

describe('IndifinishPage', () => {
  let component: IndifinishPage;
  let fixture: ComponentFixture<IndifinishPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndifinishPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IndifinishPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
