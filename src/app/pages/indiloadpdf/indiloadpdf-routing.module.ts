import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndiloadpdfPage } from './indiloadpdf.page';

const routes: Routes = [
  {
    path: '',
    component: IndiloadpdfPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IndiloadpdfPageRoutingModule {}
