import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IndiloadpdfPageRoutingModule } from './indiloadpdf-routing.module';

import { IndiloadpdfPage } from './indiloadpdf.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IndiloadpdfPageRoutingModule
  ],
  declarations: [IndiloadpdfPage]
})
export class IndiloadpdfPageModule {}
