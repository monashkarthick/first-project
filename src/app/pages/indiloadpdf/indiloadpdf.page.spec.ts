import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IndiloadpdfPage } from './indiloadpdf.page';

describe('IndiloadpdfPage', () => {
  let component: IndiloadpdfPage;
  let fixture: ComponentFixture<IndiloadpdfPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndiloadpdfPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IndiloadpdfPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
