import { Component, OnInit } from '@angular/core';
import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'indiloadpdf',
  templateUrl: './indiloadpdf.page.html',
  styleUrls: ['./indiloadpdf.page.scss'],
})
export class IndiloadpdfPage implements OnInit {

  constructor(public menuCtrl: MenuController,
    public navCtrl: NavController) { }

  ngOnInit() {
   // this.menuCtrl.enable(false);
    this.menuCtrl.swipeEnable(false);
  }

}
