import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndilogincredPage } from './indilogincred.page';

const routes: Routes = [
  {
    path: '',
    component: IndilogincredPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IndilogincredPageRoutingModule {}
