import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IndilogincredPageRoutingModule } from './indilogincred-routing.module';

import { IndilogincredPage } from './indilogincred.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IndilogincredPageRoutingModule
  ],
  declarations: [IndilogincredPage]
})
export class IndilogincredPageModule {}
