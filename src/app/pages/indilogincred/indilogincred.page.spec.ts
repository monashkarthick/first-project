import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IndilogincredPage } from './indilogincred.page';

describe('IndilogincredPage', () => {
  let component: IndilogincredPage;
  let fixture: ComponentFixture<IndilogincredPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndilogincredPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IndilogincredPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
