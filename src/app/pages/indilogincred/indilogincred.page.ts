import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { UserOptions } from '../../interfaces/user-options';
import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'indilogincred',
  templateUrl: './indilogincred.page.html',
  styleUrls: ['./indilogincred.page.scss'],
})
export class IndilogincredPage implements OnInit {
  login: UserOptions = { username: '', password: '' };
  submitted = false;
  constructor( public menuCtrl: MenuController,
    public navCtrl: NavController,
    public userData: UserData,
    public router: Router
  ) { }

  ngOnInit() {
     // this.menuCtrl.enable(false);
  }

  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.userData.login(this.login.username);
      this.router.navigateByUrl('indifinish');
    }
  }

}
