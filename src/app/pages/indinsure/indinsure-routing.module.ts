import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndinsurePage } from './indinsure.page';

const routes: Routes = [
  {
    path: '',
    component: IndinsurePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IndinsurePageRoutingModule {}
