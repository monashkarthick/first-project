import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IndinsurePageRoutingModule } from './indinsure-routing.module';

import { IndinsurePage } from './indinsure.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IndinsurePageRoutingModule
  ],
  declarations: [IndinsurePage]
})
export class IndinsurePageModule {}
