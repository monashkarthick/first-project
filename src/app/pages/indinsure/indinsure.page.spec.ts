import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IndinsurePage } from './indinsure.page';

describe('IndinsurePage', () => {
  let component: IndinsurePage;
  let fixture: ComponentFixture<IndinsurePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndinsurePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IndinsurePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
