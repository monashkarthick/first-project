import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndipdfPage } from './indipdf.page';

const routes: Routes = [
  {
    path: '',
    component: IndipdfPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IndipdfPageRoutingModule {}
