import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IndipdfPageRoutingModule } from './indipdf-routing.module';

import { IndipdfPage } from './indipdf.page';
import { FileUploadModule } from 'ng2-file-upload';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IndipdfPageRoutingModule,FileUploadModule
  ],
  declarations: [IndipdfPage]
})
export class IndipdfPageModule {}
