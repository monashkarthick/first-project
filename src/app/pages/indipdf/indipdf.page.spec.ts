import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IndipdfPage } from './indipdf.page';

describe('IndipdfPage', () => {
  let component: IndipdfPage;
  let fixture: ComponentFixture<IndipdfPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndipdfPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IndipdfPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
