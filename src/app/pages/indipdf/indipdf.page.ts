import { Component, OnInit } from '@angular/core';
import {NavController, MenuController } from '@ionic/angular';
import { Router } from '@angular/router';

import { UploadingService } from '../../providers/uploading.service';

import { FileUploader, FileLikeObject } from 'ng2-file-upload';
import { concat } from 'rxjs';
@Component({
  selector: 'indipdf',
  templateUrl: './indipdf.page.html',
  styleUrls: ['./indipdf.page.scss'],
})
export class IndipdfPage implements OnInit {
  // public fileUploader: FileUploader = new FileUploader({});
  // public hasBaseDropZoneOver: boolean = false;

  constructor( public router: Router,
    public menuCtrl: MenuController,
    public navCtrl: NavController,private uploadingService: UploadingService) { }

  ngOnInit() {
   // this.menuCtrl.enable(false);
    this.menuCtrl.swipeEnable(false);
  }
  
  // fileOverBase(event): void {
  //   this.hasBaseDropZoneOver = event;
  // }

  // getFiles(): FileLikeObject[] {
  //   return this.fileUploader.queue.map((fileItem) => {
  //     return fileItem.file;

  //   });
  // }
  



  // uploadFiles() {
   

  //   let files = this.getFiles();
  //   let requests = [];
  //   console.log("files", files);
  //   files.forEach((file) => {
  //     let formData = new FormData();
  //     formData.append('file' , file.rawFile, file.name);
  //     requests.push(this.uploadingService.uploadFormData(formData));
      
  //   });
 
  //   concat(...requests).subscribe(
  //     (res) => {
  //       console.log(res);
  //     },
  //     (err) => {  
  //       console.log(err);
  //     }
  //   );
  // }

  indilogincred()
  {
    this.router.navigateByUrl('/indilogincred');
  }
}
