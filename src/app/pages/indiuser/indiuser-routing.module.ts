import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndiuserPage } from './indiuser.page';

const routes: Routes = [
  {
    path: '',
    component: IndiuserPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IndiuserPageRoutingModule {}
