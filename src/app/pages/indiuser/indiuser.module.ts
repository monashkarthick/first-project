import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IndiuserPageRoutingModule } from './indiuser-routing.module';

import { IndiuserPage } from './indiuser.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IndiuserPageRoutingModule
  ],
  declarations: [IndiuserPage]
})
export class IndiuserPageModule {}
