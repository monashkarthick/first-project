import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IndiuserPage } from './indiuser.page';

describe('IndiuserPage', () => {
  let component: IndiuserPage;
  let fixture: ComponentFixture<IndiuserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndiuserPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IndiuserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
