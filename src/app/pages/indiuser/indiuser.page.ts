import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { UserOptions, UserIndi } from '../../interfaces/user-options';
import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'indiuser',
  templateUrl: './indiuser.page.html',
  styleUrls: ['./indiuser.page.scss'],
})
export class IndiuserPage implements OnInit {
  indiuser: UserIndi = { email: '', number: '' };
  submitted = false;
  constructor(
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public userData: UserData,
    public router: Router
  ) { }

  ngOnInit() {
     // this.menuCtrl.enable(false);
  }

  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.userData.login(this.indiuser.email);
      this.router.navigateByUrl('indiusertype');
    }
  }

}
