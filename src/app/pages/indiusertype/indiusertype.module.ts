import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IndiusertypePageRoutingModule } from './indiusertype-routing.module';

import { IndiusertypePage } from './indiusertype.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IndiusertypePageRoutingModule
  ],
  declarations: [IndiusertypePage]
})
export class IndiusertypePageModule {}
