import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IndiusertypePage } from './indiusertype.page';

describe('IndiusertypePage', () => {
  let component: IndiusertypePage;
  let fixture: ComponentFixture<IndiusertypePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndiusertypePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IndiusertypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
