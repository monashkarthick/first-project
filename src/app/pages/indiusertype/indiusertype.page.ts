import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { UserOptions, UserOpt } from '../../interfaces/user-options';
import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'indiusertype',
  templateUrl: './indiusertype.page.html',
  styleUrls: ['./indiusertype.page.scss'],
})
export class IndiusertypePage implements OnInit {
  usertype: UserOpt = { name: '', number: '', contact:'' };
  submitted = false;
  constructor(
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public userData: UserData,
    public router: Router
  ) { }

  ngOnInit() {
      // this.menuCtrl.enable(false);
  }

  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.userData.login(this.usertype.name);
      this.router.navigateByUrl('indipdf');
    }
  }

}
