import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LogincredPage } from './logincred.page';

const routes: Routes = [
  {
    path: '',
    component: LogincredPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LogincredPageRoutingModule {}
