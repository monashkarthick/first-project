import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LogincredPageRoutingModule } from './logincred-routing.module';

import { LogincredPage } from './logincred.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LogincredPageRoutingModule
  ],
  declarations: [LogincredPage]
})
export class LogincredPageModule {}
