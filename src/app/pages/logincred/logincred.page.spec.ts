import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LogincredPage } from './logincred.page';

describe('LogincredPage', () => {
  let component: LogincredPage;
  let fixture: ComponentFixture<LogincredPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogincredPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LogincredPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
