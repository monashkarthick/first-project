import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MathomePage } from './mathome.page';

const routes: Routes = [
  {
    path: '',
    component: MathomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MathomePageRoutingModule {}
