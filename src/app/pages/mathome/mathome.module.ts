import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MathomePageRoutingModule } from './mathome-routing.module';

import { MathomePage } from './mathome.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MathomePageRoutingModule
  ],
  declarations: [MathomePage]
})
export class MathomePageModule {}
