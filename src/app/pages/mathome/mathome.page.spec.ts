import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MathomePage } from './mathome.page';

describe('MathomePage', () => {
  let component: MathomePage;
  let fixture: ComponentFixture<MathomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MathomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MathomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
