import { Component, OnInit } from '@angular/core';

import {NavController, MenuController } from '@ionic/angular';

@Component({
  selector: 'patient',
  templateUrl: './patient.page.html',
  styleUrls: ['./patient.page.scss'],
})
export class PatientPage implements OnInit {
  

  constructor(public menuCtrl: MenuController,
    public navCtrl: NavController) { }

  ngOnInit() {
   // this.menuCtrl.enable(false);
    this.menuCtrl.swipeEnable(false);
  }
  patientnotes(){
    this.navCtrl.navigateRoot('/patientnotes');

  }

}
