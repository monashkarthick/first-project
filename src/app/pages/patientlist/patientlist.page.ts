import { Component, OnInit } from '@angular/core';

import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'patientlist',
  templateUrl: './patientlist.page.html',
  styleUrls: ['./patientlist.page.scss'],
})
export class PatientlistPage implements OnInit {
 
  constructor(public menuCtrl: MenuController,
    public navCtrl: NavController) { }

  ngOnInit() {
   // this.menuCtrl.enable(false);
    this.menuCtrl.swipeEnable(false);
  }
  
    
  
patient(){
  this.navCtrl.navigateRoot('/patient'); 
}
child1(){
  this.navCtrl.navigateRoot('/child1'); 
}
child2(){
  this.navCtrl.navigateRoot('/child2'); 
}
login(){
  this.navCtrl.navigateRoot('/'); 
}
}
