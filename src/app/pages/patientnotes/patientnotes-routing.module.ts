import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatientnotesPage } from './patientnotes.page';

const routes: Routes = [
  {
    path: '',
    component: PatientnotesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatientnotesPageRoutingModule {}
