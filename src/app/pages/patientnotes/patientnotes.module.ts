import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatientnotesPageRoutingModule } from './patientnotes-routing.module';

import { PatientnotesPage } from './patientnotes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PatientnotesPageRoutingModule
  ],
  declarations: [PatientnotesPage]
})
export class PatientnotesPageModule {}
