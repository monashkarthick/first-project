import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PatientnotesPage } from './patientnotes.page';

describe('PatientnotesPage', () => {
  let component: PatientnotesPage;
  let fixture: ComponentFixture<PatientnotesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientnotesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PatientnotesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
