import { Component, OnInit } from '@angular/core';
import {NavController, MenuController } from '@ionic/angular';
@Component({
  selector: 'patientnotes',
  templateUrl: './patientnotes.page.html',
  styleUrls: ['./patientnotes.page.scss'],
})
export class PatientnotesPage implements OnInit {

  constructor(public menuCtrl: MenuController,
    public navCtrl: NavController) { }

  ngOnInit() {
   // this.menuCtrl.enable(false);
    this.menuCtrl.swipeEnable(false);
  }

}
