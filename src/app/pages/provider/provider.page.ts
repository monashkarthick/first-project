import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { UserOptions, UserPovider } from '../../interfaces/user-options';
import {NavController, MenuController } from '@ionic/angular';

@Component({
  selector: 'provider',
  templateUrl: './provider.page.html',
  styleUrls: ['./provider.page.scss'],
})
export class ProviderPage implements OnInit {
  provider: UserPovider = { email: '', password: '', registration:'', name:'' };
  submitted = false;

  constructor(
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public userData: UserData,
    public router: Router
  ) { }

  ngOnInit() {
    // this.menuCtrl.enable(false);
  }
  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.userData.login(this.provider.email);
      this.router.navigateByUrl('providertype');
    }
  }

  



}
