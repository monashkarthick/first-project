import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProvidercheckPageRoutingModule } from './providercheck-routing.module';

import { ProvidercheckPage } from './providercheck.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProvidercheckPageRoutingModule
  ],
  declarations: [ProvidercheckPage]
})
export class ProvidercheckPageModule {}
