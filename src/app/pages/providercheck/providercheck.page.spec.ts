import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProvidercheckPage } from './providercheck.page';

describe('ProvidercheckPage', () => {
  let component: ProvidercheckPage;
  let fixture: ComponentFixture<ProvidercheckPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvidercheckPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProvidercheckPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
