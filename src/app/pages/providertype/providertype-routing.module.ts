import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProvidertypePage } from './providertype.page';

const routes: Routes = [
  {
    path: '',
    component: ProvidertypePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProvidertypePageRoutingModule {}
