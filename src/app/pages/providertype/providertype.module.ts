import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProvidertypePageRoutingModule } from './providertype-routing.module';

import { ProvidertypePage } from './providertype.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProvidertypePageRoutingModule
  ],
  declarations: [ProvidertypePage]
})
export class ProvidertypePageModule {}
