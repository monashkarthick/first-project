import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProvidertypePage } from './providertype.page';

describe('ProvidertypePage', () => {
  let component: ProvidertypePage;
  let fixture: ComponentFixture<ProvidertypePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvidertypePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProvidertypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
