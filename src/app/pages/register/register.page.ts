import { Component, OnInit } from '@angular/core';

import { NavController, MenuController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
 
 
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
  
  ) { }

  ionViewWillEnter() {
    // this.menuCtrl.enable(false);
  }

  ngOnInit() {

  }


  goToLogin() {
    this.navCtrl.navigateRoot('/login');
  }
  individual() {
    this.navCtrl.navigateRoot('/individual');
  }
  business() {
    this.navCtrl.navigateRoot('/business');
  }
}
